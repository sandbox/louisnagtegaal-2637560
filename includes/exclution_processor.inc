<?php
/**
 * Created by PhpStorm.
 * User: louis
 * Date: 20-11-15
 * Time: 16:40
 */


class SearchApiExclutionProcessor extends SearchApiAbstractAlterCallback {
  /**
   * Alter the items into there respective groups.
   */
  public function alterItems(array &$items) {
    // get all nids to exclude
    $excluded =  db_select('search_api_exclution_nids', 'e')
      ->fields('e', array('nid'))
      ->execute()
      ->fetchAll();
    foreach ($excluded as $exitem) {
      unset($items[$exitem->nid]);
    }
  }

  /**
   * Processor details.
   */
  public function propertyInfo() {
    return array(
      'exclution' => array(
        'label' => t('Exclude specific nodes'),
        'description' => t('Exclude the node if this is selected on the node form.'),
        'type' => 'string',
      ),
    );
  }
}
