<?php

/**
 * @file
 * SearchAPI indexation selector
 */


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 * Add extra field to exclude node from SearchAPI index
 */
function searchapi_exclution_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node_edit_form']) && $form['#node_edit_form']) {
    $node = $form['#node'];
    if (_searchapi_exclution_active($node->type)) {
      $default = false;
      if(!empty($node->nid)){
        $default = searchapi_exclution_get($node->nid);
      }
      // add checkbox in the vertical tab of the publish options
      $form['options']['search_api_index_status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Exclude from search index.'),
        '#weight' => 1,
        '#default_value' => $default,
      );
      // add extra submit function, we do not use
      //     array_unshift($form['#submit'], 'searchapi_exclution_submit');
      // because then we do not yet have a nid if the submit function is called
      // see  https://www.drupal.org/node/871138#comment-4648964
      $form['actions']['submit']['#submit'][] = 'searchapi_exclution_submit';
    }
  }
}

/**
 * @param $node
 * @return bool
 * Helper function to check if for given node the exclution is enabled
 */
function _searchapi_exclution_active($type) {
  $is_active = false;
  // get all types from the database
  $result = db_select('searchapi_exclution_types', 't')
    ->fields('t', array('id', 'node_type'))
    ->execute();
  // loop over db rsults
  foreach($result as $row){
    // if type is in database result
    if ($row->node_type == $type) {
      $is_active = true;
      break;
    }
  }
  return $is_active;
}

/**
 * @param $form
 * @param $form_state
 * @throws \Exception
 * Submit function to save values to database
 */
function searchapi_exclution_submit($form, &$form_state) {
  // set default index, is not used now
  $index = 'node_index';
  // get value to save
  $value = $form_state['values']['search_api_index_status'];
  // get node ID to save fori
  $nid = $form_state['values']['nid'];
  // remove value from database
  if(!empty($nid)){
    db_delete('searchapi_exclution_nids')
      ->condition('nid', $nid)
      ->execute();
    // is checkbox is set, insert new value into database
    if (!empty($value)) {
      // else insert
      db_insert('searchapi_exclution_nids')
        ->fields(array(
          'nid' => $nid,
          'index_name' => $index,
        ))->execute();
    }
    search_api_track_item_change('node', array($nid));
  }
}

/**
 * @param $nid
 * @return bool
 * Helper function to check if node with given ID must be excluded
 */
function searchapi_exclution_get($nid) {
  $is_excluded = FALSE;
  $query = db_select('searchapi_exclution_nids', 'i')
    ->fields('i', array('nid', 'index_name'))
    ->condition('nid', $nid)
    ->range(0, 1)
    ->execute();
  $result = $query->fetchObject();
  if ($result) {
    $is_excluded = TRUE;
  }
  return $is_excluded;
}

/**
 * Implements hook_permission().
 */
function searchapi_exclution_permission() {
  $return = array();

  $return['administer searchapi exclution'] = array(
    'title' => t('Administer SearchAPI exclusion'),
    'description' => t('Allow to the user to administer SearchAPI exclusion settings.'),
  );
  return $return;
}

/**
 * Implements hook_menu().
 */
function searchapi_exclution_menu() {
  $items = array();

  $items['admin/config/search/searchapi_exclude'] = array(
    'title' => 'SearchAPI exclude settings',
    'description' => 'Determine which node-types must have exclution option',
    'access callback' => 'user_access',
    'access arguments' => array('administer searchapi exclution'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('searchapi_exclution_settings_form'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * @return array
 * Settings form for SearchAPI exclution
 */
function searchapi_exclution_settings_form() {
  $form = array();

  $form['searchapi_exclution_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types with exclution option'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['searchapi_exclution_types']['types'] = array(
    '#type' => 'checkboxes',
    '#options' => _searchapi_exclution_get_node_types_options(),
    '#default_value' => _searchapi_exclution_get_types(),
    '#title' => t('Node types which can use the exclude option'),
  );

  // Short help Index items immediately
  $form['searchapi_exclution_types']['help'] = array(
    '#markup' => t("Make sure the option 'Index items immediately' is set for the current search index to ensure immediate result when a node is flagged as excluded.<br/>
 If this option is not set the result will only be visible when cron has run and the index is updated."),
  );
  $form = system_settings_form($form);
  // add submit function to sve the values
  array_unshift($form['#submit'], 'searchapi_exclution_settings_form_submit');
  return $form;
}

function _searchapi_exclution_get_node_types_options() {
  $options = array();
  $all_types = node_type_get_types();
  foreach($all_types as $type=>$object){
    $options[$type] = $object->name;
  }
  return $options;
}

function _searchapi_exclution_get_types() {
  $types = array();
  $results = db_select('searchapi_exclution_types', 't')
    ->fields('t', array('node_type'))
    ->execute()
    ->fetchAll();
  foreach ($results as $record) {
    $types[$record->node_type] = $record->node_type;
  }
  return $types;
}

function searchapi_exclution_settings_form_submit($form, &$form_state){
  foreach($form_state['values']['types'] as $key => $type){
    db_delete('searchapi_exclution_types')
      ->condition('node_type', $key)
      ->execute();
    if(!empty($type)){
      db_insert('searchapi_exclution_types')
        ->fields(array(
          'node_type' => $type,
        ))->execute();
    }
  }
}

/**
 * Implements hook_search_api_alter_callback_info().
 */
function searchapi_exclution_search_api_alter_callback_info() {
  $callbacks = array();
  $callbacks['searchapi_exclution'] = array(
    'name' => t('Exclude selected nodes'),
    'description' => t('Exclude nodes which are selected to be exclude on the  node edit form.'),
    'class' => 'SearchApiExclutionProcessor',
  );
  return $callbacks;
}

/**
 * Implements hook_node_delete().
 * Remove id from database if node is removed
 */
function searchapi_exclution_node_delete($node) {
  $nid = $node->nid;
  if(!empty($nid)) {
    db_delete('searchapi_exclution_nids')
      ->condition('nid', $nid)
      ->execute();
  }
}
